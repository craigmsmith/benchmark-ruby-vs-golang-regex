package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"testing"
)

func Benchmark10RandomFiles(b *testing.B) {
	gl := ParseTOML("gitleaks.toml")
	Benchmarkfile(gl, b)
}

func Benchmarkfile(gl *Gitleaks, b *testing.B) {
	filesData := make([][]byte, 0)
	testdataPath := "../LoremIpsumFiles"
	files, err := os.ReadDir(testdataPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		filePath := fmt.Sprintf("%s/%s", testdataPath, file.Name())
		content, _ := readFile(filePath)
		filesData = append(filesData, content)
	}

	searcher := NewSearcher(gl)
	b.ResetTimer()
	for _, fileData := range filesData {
		result := searcher.Search(fileData)
		if result == false {
			panic("All files include a token")
		}
	}
}

func readFile(filename string) ([]byte, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var content []byte
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		content = append(content, scanner.Bytes()...)
		content = append(content, '\n') // add newline after each line
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return content, nil
}
