module regex_performance

go 1.19

require github.com/pelletier/go-toml/v2 v2.1.0

require (
	github.com/magefile/mage v1.14.0 // indirect
	github.com/tetratelabs/wazero v1.3.1 // indirect
	github.com/wasilibs/go-re2 v1.4.0 // indirect
)
