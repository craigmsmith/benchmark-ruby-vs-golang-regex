package main

import re2 "github.com/wasilibs/go-re2"

type RE2Searcher struct {
	regexarr []*re2.Regexp
}

func NewRE2Searcher(gl *Gitleaks) *RE2Searcher {
	arr := make([]*re2.Regexp, 0, len(gl.Rules))
	for _, rule := range gl.Rules {
		r := re2.MustCompile(rule.Regex)
		arr = append(arr, r)
	}
	return &RE2Searcher{arr}
}

func (s RE2Searcher) Search(fileData []byte) bool {
	match := false
	for _, rule := range s.regexarr {
		if rule.Match(fileData) {
			// Don't return when found because we want to search with every expression, for benchmarking
			match = true
		}
	}
	return match
}
