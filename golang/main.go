package main

import (
	"fmt"
	"os"
	"regexp"

	"github.com/pelletier/go-toml/v2"
)

type Gitleaks struct {
	Title string
	Rules []GitLeakRule
}

type GitLeakRule struct {
	Regex string
}

type Searcher struct {
	regexarr []*regexp.Regexp
}

func NewSearcher(gl *Gitleaks) *Searcher {
	arr := make([]*regexp.Regexp, 0, len(gl.Rules))
	for _, rule := range gl.Rules {
		r := regexp.MustCompile(rule.Regex)
		arr = append(arr, r)
	}
	return &Searcher{arr}
}

func (s Searcher) Search(fileData []byte) bool {
	match := false
	for _, rule := range s.regexarr {
		if rule.Match(fileData) {
			// Don't return when found because we want to search with every expression, for benchmarking
			match = true
		}
	}
	return match
}

func ParseTOML(filePath string) *Gitleaks {
	// parse
	f, err := os.ReadFile(fmt.Sprintf("../%s", filePath))
	if err != nil {
		panic(err)
	}

	gl := &Gitleaks{}
	err = toml.Unmarshal(f, &gl)
	if err != nil {
		panic(err)
	}

	return gl
}
