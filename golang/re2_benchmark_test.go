package main

import (
	"fmt"
	"log"
	"os"
	"testing"
)

func BenchmarkRE2(b *testing.B) {
	gl := ParseTOML("gitleaks.toml")
	BenchmarkfileRE2(gl, b)
}

func BenchmarkfileRE2(gl *Gitleaks, b *testing.B) {
	filesData := make([][]byte, 0)
	testdataPath := "../LoremIpsumFiles"
	files, err := os.ReadDir(testdataPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		filePath := fmt.Sprintf("%s/%s", testdataPath, file.Name())
		content, _ := readFile(filePath)
		filesData = append(filesData, content)
	}

	searcher := NewRE2Searcher(gl)
	b.ResetTimer()
	for _, fileData := range filesData {
		result := searcher.Search(fileData)
		if result == false {
			panic("All files include a token")
		}
	}
}
