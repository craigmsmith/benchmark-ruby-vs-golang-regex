require 'toml-rb'
require 're2'
require 'benchmark'

class SecretFinder
  def initialize(gitleaks_filename)
		path = "../#{gitleaks_filename}"
    data = TomlRB.load_file(path)

		@re_set = RE2::Set.new
    data['rules'].each do |rule|
      pattern = rule['regex']
			@re_set.add(pattern)
		end
    puts "failed to compile regex patterns" if !@re_set.compile
	end

	def search_using_set(data)
    found = false
    data.each do |line|
      match = @re_set.match(line)
      if match[0]
        found = true
      end
    end
    return found
  end
end

finder = SecretFinder.new("gitleaks.toml")

files = []
directory = Pathname.new(File.join('../', 'LoremIpsumFiles'))
directory.each_child do |entry|
  if entry.file?
    puts "File: #{entry.basename}"
    files.append(IO.readlines(entry.expand_path))
  end
end

Benchmark.bm() do |x|
  x.report("10 Random files") do
    files.each do |f|
      found = finder.search_using_set(f)
      unless found
        raise 'Something should have been found!'
      end
    end
  end
end

