# Benchmark Ruby vs Golang Regex

## Overview
This project aims to benchmark and compare the performance of regular expressions in two programming languages: Ruby (utilizing the re2 bindings) and Golang (using the standard Golang regex library). 

## Methodology
The purpose of this project is to only compare regular expression performance, this file IO of reading and parsing the regular expression TOML should not be included, nor should the IO time taken to read the files to be searched.

To avoid caching benifits that may occur by parsing the same file multiple times, each file is searched only once, and all files are unique, containing random lorum ipsum. 

## Results

<table>
<tr>
<th>Golang Seconds</th>
<th>Ruby Seconds</th>
</tr>
<tr>
<td>7.863387583 seconds</td>
<td>0.060479 seconds</td>
</tr>
</table>
